
import logging
import pika
import tornado
import ujson


class Messageconnector(object):

    
    def __init__(self, username, password, host, port, vhost):
        credentials = pika.PlainCredentials(username, password)
        conn_params = pika.ConnectionParameters(host, port, vhost,
                                        credentials=credentials)                                        
        self.connection = pika.TornadoConnection(conn_params,
        on_open_callback=self.on_connect)
        self.connection.add_on_close_callback(self.on_closed)

# make message persistent
    def sendmessage(self, match_id, message):
        self.channel.basic_publish(exchange='cricket-live',
                      routing_key=match_id,
                      body=ujson.dumps(message),
                      properties=pika.BasicProperties(
                         delivery_mode=2,
                      ))


    def on_connect(self, connection):
        self.connection = connection
        connection.channel(self.on_channel_open)
 
    def on_channel_open(self, channel):
        self.channel = channel
        self.channel.exchange_declare(exchange='cricket-live',
                         type='direct', passive=False,
                         auto_delete=False)
        logging.info('Channel Open for Rabbitmq')

    def on_exchange_declare(self, frame):
        logging.info("Exchange declared.")
 
    def on_basic_cancel(self, frame):
        logging.info('Basic Cancel Ok.')
        self.connection.close()
 
    def on_closed(self, connection):
        logging.error("Issue with eventloop for rabbitmq")

    def close(self):
        self.channel.close()
        
    def __exit__(self, _type, value, traceback):
        self.close()
        

