from distutils.core import setup

# sudo python setup.py install

setup(

    name="Wappez Cricket Service",

    # Version number (initial):
    version="0.1.0",

    # Application author details:
    author="mithran kulasekaran",
    author_email="akmithu91@gmail.com",

    # Packages
    packages=["cacheconnector", "scheduler", "testcode", "config", "messageconnector", "webserver"],
    include_package_data=True,

    description="Cricket Game Schedular",
    install_requires=[
        "ujson", "feedparser", "tornado", "pytz", "pika", "tornadoredis"
    ],
)
