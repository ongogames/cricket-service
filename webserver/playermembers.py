'''
Created on Jun 1, 2015

@author: mithran
'''
from webserver.requestbase import RequestBase


class PlayerMembers(RequestBase):
    
    __slots__ = ['config', 'matchmap']

    def initialize(self, config, matchmap):
        self.config = config
        self.matchmap = matchmap        
   

    def get(self, match_id):
        if match_id in self.matchmap:            
            responsebody = self.matchmap[match_id].getresponse()
            team1 = responsebody["team"][0]
            team2 = responsebody["team"][1]
            squad_team1 = team1.get("squad", None)
            squad_team2 = team2.get("squad", None)
            player_team1 = team1.get("player", None)
            player_team2 = team2.get("player", None)
            if (squad_team1 != None and squad_team2 != None):
                self.finish({"is_squad":True, "squad":[{"team":team1["team_general_name"], "players":self.getsquads(squad_team1)},
                                       {"team":team2["team_general_name"], "players":self.getsquads(squad_team2)}]})
            elif (player_team1 != None and player_team2 != None):    
                self.finish({"is_squad":False, "squad":[{"team":team1["team_general_name"], "players":self.getplayers(player_team1)},
                                       {"team":team2["team_general_name"], "players":self.getplayers(player_team2)}]})
            else:
                self.set_status(204)
                self.finish()
        else:
            self.set_status(404, "Invalid matchid")
            self.finish()
                
    def getsquads(self, squad_team):
        return [squad["known_as"] for squad in squad_team]
                
    def getplayers(self, player_team):
        players = []
        for player in player_team:
            details = {}
            if player["captain"] == u"1":
                details["captain"] = True
            if player["keeper"] == u"1":    
                details["keeper"] = True
            details["name"] = player["known_as"]
            role = player.get("player_primary_role", None)
            if role != None:
                details["role"] = role
            players.append(details)
        return players
