'''
Created on Jun 7, 2015

@author: mithran
'''
import tornado


class HealthCheck(tornado.web.RequestHandler):
    
    def get(self):
        self.finish("OK")