'''
Created on Jun 1, 2015

@author: mithran
'''
from webserver.requestbase import RequestBase


class CheckToss(RequestBase):

    __slots__ = ['config', 'matchmap']

    def initialize(self, config, matchmap):
        self.config = config
        self.matchmap = matchmap        


    def get(self, match_id):    
        if match_id in self.matchmap:            
            team = {}
            responsebody = self.matchmap[match_id].getresponse()
            team[responsebody["team"][0]["team_id"]] = responsebody["team"][0]["team_general_name"]
            team[responsebody["team"][1]["team_id"]] = responsebody["team"][1]["team_general_name"]
            tosswinner = responsebody["match"].get("toss_winner_team_id", None)

            if tosswinner == None:
                self.finish("Toss not started")
            else:
                self.finish({"toss": responsebody["match"]["toss_decision_name"], "team":team[tosswinner]})    
        else:
            self.set_status(404, "Invalid matchid")
            self.finish()
