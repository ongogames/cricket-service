'''
Created on Jun 4, 2015

@author: mithran
'''

from webserver.requestbase import RequestBase


class MatchStatus(RequestBase):
    
    __slots__ = ['config', 'matchmap']

    def initialize(self, config, matchmap):
        self.config = config
        self.matchmap = matchmap        
        
    def get(self, match_id):    
        if match_id in self.matchmap:                        
            responsebody = self.matchmap[match_id].getresponse()
            
            match_live_status = responsebody["live"].get("status", None)
            match_break = responsebody["live"].get("break", None)

            current_summary = responsebody["match"].get("current_summary", None)  
            current_result = responsebody["match"].get("result_name", None)  
            winner_team_id = responsebody["match"].get("winner_team_id", None)  
            match_hours = responsebody["match"].get("hours_string", None)
            
            self.finish({"match_summary":current_summary, "match_result":current_result,
                         "winner_id":winner_team_id, "live_status":match_live_status, "match_break":match_break,
                         "match_hours":match_hours})
        else:
            self.set_status(404, "Invalid matchid")
            self.finish()
