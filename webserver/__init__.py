

from config import Configuration
from messageconnector import Messageconnector
from scheduler import CricketSchedular
from scheduler.helper import getfilecontents, getargs
import sys
import tornado
from tornado.options import define, options
import ujson, logging
from webserver.ballstatus import BallStatus
from webserver.checktoss import CheckToss
from webserver.healthcheck import HealthCheck
from webserver.livematch import LiveMatches
from webserver.matchstats import MatchStats
from webserver.matchstatus import MatchStatus
from webserver.playermembers import PlayerMembers
from webserver.private import ListMatches, StopMatch

# "guest", "guest", "localhost", 5672, "/"
def get_rabbitmq_connection(config):
    return Messageconnector(config.get_rabbitmq_username(), config.get_rabbitmq_password(),
                            config.get_rabbitmq_host(), config.get_rabbitmq_port(),
                            config.get_rabbitmq_vhost())        


def startserver():
    matchmap = {}

    config = getfilecontents(getargs())
    config = ujson.loads(config)
    config = Configuration(config)
    
    logpath = config.get_logpath()
    
    rabbit_connection = get_rabbitmq_connection(config)
    args = []
#     tornado.log.LogFormatter(color=True, fmt='[%(levelname) %(asctime)s ] %(message)s', datefmt='%y%m%d %H:%M:%S')
    args.append("--logging=debug")
    args.append("--log_file_prefix=" + logpath)
    args.append("--log_to_stderr="+ str(config.get_log_stderr()))
    args.append("--log_file_max_size=" + str(config.get_log_filesize()))
    tornado.options.parse_command_line(args)
    
    configuration = {"config":config, "matchmap":matchmap}
    liveconfig = configuration.copy()
    liveconfig["messageconnector"] = rabbit_connection
    
    application = tornado.web.Application([(r"/cricket/live_matches", LiveMatches, liveconfig),
                                           
                                           (r"/cricket/squad/([0-9]+)", PlayerMembers, configuration),
                                           (r"/cricket/toss/([0-9]+)", CheckToss, configuration),
                                           (r"/cricket/stats/([0-9]+)", MatchStats, configuration),
                                           (r"/cricket/status/([0-9]+)", MatchStatus, configuration),
                                           (r"/cricket/ballstatus/([0-9]+)", BallStatus, configuration),
                                           
                                           
                                           (r"/private/list_matches", ListMatches, {"matchmap":matchmap}),
                                           (r"/private/stop_match/([0-9]+)", StopMatch, {"matchmap":matchmap}),
                                           
                                           (r"/healthcheck", HealthCheck)   ,
                                           
                                           (r'/cricket/static/(.*)', tornado.web.StaticFileHandler, {'path': config.get_static_path()})
                                           ])

    logging.info("Server Started at port {0}".format(config.get_port()))
    
    application.listen(config.get_port())
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    startserver()
