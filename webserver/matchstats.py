
'''
Created on Jun 1, 2015

@author: mithran
'''
from webserver.requestbase import RequestBase


class MatchStats(RequestBase):   
    
    __slots__ = ['config', 'matchmap']

    def initialize(self, config, matchmap):
        self.config = config
        self.matchmap = matchmap        

    def get(self, match_id):    

        if match_id in self.matchmap:                        
            responsebody = self.matchmap[match_id].getresponse()
            matchstatus = responsebody["live"]["status"]  
            description = responsebody["description"]  
            weather_description = responsebody["weather"]["observation"]["description"]
            weather_c = responsebody["weather"]["observation"]["temperature_c"] 
            ground_countryname = responsebody["match"]["country_name"]
            match_title = responsebody["match"]["cms_match_title"] 
            groundname = responsebody["match"]["ground_name"]
            self.finish({"status":matchstatus, "description":description, "groundname":groundname,
                          "weather_description":weather_description, "weather_c":weather_c,
                          "ground_countryname":ground_countryname, "match_title":match_title, })
        else:
            self.set_status(404, "Invalid matchid")
            self.finish()
       
