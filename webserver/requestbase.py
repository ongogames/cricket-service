'''
Created on Jun 7, 2015

@author: mithran
'''
import logging
import tornado


class RequestBase(tornado.web.RequestHandler):
    
    def handle_request(self, response):
        if response.error:
            logging.error(response.error)
        else:
            logging.info("{0}:{1}".format(response.effective_url, response.code))    

    
    def set_default_headers(self):
        self.clear_header("Server")
        self.set_header("cache-control", "public, max-age=20")

        
