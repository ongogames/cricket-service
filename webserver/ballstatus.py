'''
Created on Jun 7, 2015

@author: mithran
'''

from webserver.requestbase import RequestBase

class BallStatus(RequestBase):
    
    
    __slots__ = ['config', 'matchmap']

    def initialize(self, config, matchmap):
        self.config = config
        self.matchmap = matchmap        
    
    
    def get(self, match_id):
        if match_id in self.matchmap:                        
            match = self.matchmap[match_id]
            self.finish(match.get_ball_status())
        else:
            self.set_status(404, "Invalid matchid")
            self.finish()
            
