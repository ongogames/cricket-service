'''
Created on Jun 7, 2015

@author: mithran
'''
import tornado


class StopMatch(tornado.web.RequestHandler):
    
    def initialize(self, matchmap):
        self.matchmap = matchmap

    def get(self, match_id):    
        if match_id in self.matchmap:                        
            match = self.matchmap[match_id]
            match.stopschedular()
            del self.matchmap[match_id]
        self.finish()


class ListMatches(tornado.web.RequestHandler):
    
    def initialize(self, matchmap):
        self.matchmap = matchmap

#     @tornado.web.asynchronous
    def get(self):    
        self.finish({"match_ids":self.matchmap.keys()})
        
        
