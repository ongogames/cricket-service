'''
Created on Jun 1, 2015

@author: mithran
'''
import calendar
from datetime import datetime
import feedparser
from pytz import timezone
import re
from scheduler import CricketSchedular
import sys
import time
import tornado
from tornado.httpclient import HTTPRequest
import ujson
from webserver.requestbase import RequestBase


class LiveMatches(RequestBase):
    
    
    __slots__ = ['config', 'matchmap', 'messageconnector', 'response']

    def initialize(self, config, matchmap, messageconnector):
        self.config = config
        self.mastermap = matchmap 
        self.messageconnector = messageconnector
        self.response = None       


    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self):
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = yield http_client.fetch(HTTPRequest(self.config.get_live_url()), self.handle_request, raise_error=False)
        if not response.error:
            if response.code == 304 and self.response != None:
                self.finish(self.response)
            else:
                matches = self.getresponse(response)
                urls = []
                matchids = set()
                for match in matches:
                    matchid = str(match["match_id"])
                    matchids.add(matchid)
                    url = self.config.get_root_url() + str(match["match_id"]) + ".json"
                    urls.append(HTTPRequest(url, connect_timeout=5000, request_timeout=5000))
                        
                
                results = yield [tornado.gen.Task(http_client.fetch, url) for url in urls] 
                index = 0
                
                for  match in results:
                    response = ujson.loads(match.body)
                    
                    futuretime = self.getfuturematch(response["match"].get("next_datetime_gmt", None))
                    
                    matchid = matches[index]["match_id"]
                    
                    self.mastermap[matchid] = CricketSchedular(self.config, matchid, futuretime, response, self.messageconnector)
                    
                    team1_path = response["team"][0]["logo_image_path"]
                    name1 = response["team"][0]["team_general_name"]
                    url1 = self.config.get_root() + team1_path
                    url1 = HTTPRequest(url1, body="byte", connect_timeout=self.config.get_connect_timeout(), request_timeout=self.config.get_request_timeout())
    
                    team2_path = response["team"][1]["logo_image_path"]
                    name2 = response["team"][1]["team_general_name"] 
                    url2 = self.config.get_root() + team2_path
                    url2 = HTTPRequest(url2, connect_timeout=self.config.get_connect_timeout(), request_timeout=self.config.get_request_timeout())  
                    
                    if team1_path != "" and team2_path != "":
                        team1_response, team2_response = yield [tornado.gen.Task(http_client.fetch, url1),
                                                                 tornado.gen.Task(http_client.fetch, url2)]
                        team1_size = sys.getsizeof(team1_response.body)
                        team2_size = sys.getsizeof(team2_response.body)
                        if team1_size > 20 :
                            matches[index][name1] = self.config.get_rootstatic_path() + self.getimage(team1_response)
                        if team2_size > 20:
                            matches[index][name2] = self.config.get_rootstatic_path() + self.getimage(team2_response)
                        
                    matches[index]["description"] = response["description"]
    
                    if futuretime > calendar.timegm(time.gmtime()):
                        matches[index]["starts_at"] = futuretime
    
                    index += 1
                
                mapkeys = set(self.mastermap.keys())
                removekeys = mapkeys.difference(matchids) 
                for key in removekeys:
                    match_id = self.mastermap[key]
                    match_id.stopschedular()
                    del self.mastermap[key]
                
                self.set_header("cache-control", "public, max-age=" + str(self.config.get_varnish_live_request()))
                self.response = {"matches":matches}
                self.finish(self.response)
        else:
            self.set_status(204)
            self.finish()    
    
    
    def getimage(self, response):
        url = response.effective_url
        frags = url.split("/")
        img = frags[len(frags) - 1]
        with open(self.config.get_static_path() + "images/" + img, 'wb') as output:
            output.write(bytes(response.body))
        return img
        
    def getresponse(self, response):
        response_feed = feedparser.parse(response.body)
        feeds = response_feed['entries']
        matches = []
        for feed in feeds:
            feed_id = re.findall(r'\d+', feed['id'])[0]
            feed_title = re.sub(r'[0-9\*//]', r'', feed['title'])
            if feed_id not in self.mastermap:
                matches.append({"match_id":feed_id, "title":(re.sub('[^A-Za-z0-9 ]+', '', feed_title))})
        return matches

    def getfuturematch(self, value):
        if value != None :
            if ":" in value:
                formatTemplate = "%Y-%m-%d %H:%M:%S"                
            else:
                formatTemplate = "%Y-%m-%d"
            t = datetime.strptime(value, formatTemplate)
            ty = t.replace(tzinfo=timezone('GMT'))
            return calendar.timegm(ty.timetuple())

    """
    Calculated time diff for current match
    """
    def timediff(self, hour, minutes):
        epochtime = calendar.timegm(time.gmtime())
        dt = datetime.fromtimestamp(epochtime)
        adt = dt.replace(hour=hour, minute=minutes)
        return calendar.timegm(adt.timetuple())    
