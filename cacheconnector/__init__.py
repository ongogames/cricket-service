import logging
import sys
from tornado import gen
import tornado
import tornadoredis


class Cachemanager(object):
    
    __slots__ = ['client', 'livekey']
    
    def __init__(self, max_connections, host, port, livekey, addteam):
        connection_pool = tornadoredis.ConnectionPool(max_connections=max_connections, wait_for_available=True)
        self.client = tornadoredis.Client(connection_pool=connection_pool, host=host, port=port)
        self.livekey = livekey
        self.addteam = addteam

    @gen.coroutine
    def putlive(self, matches):
        yield tornado.gen.Task(self.client.evalsha, self.livekey, [], matches)

    @gen.coroutine
    def addTeam(self, teamname, teamid):
        yield tornado.gen.Task(self.client.set, teamname, teamid)
    
    
    @gen.coroutine     
    def addteamplayers(self, team_id, teamplayers):
        yield tornado.gen.Task(self.client.evalsha, self.addteam, [team_id], teamplayers)
        
                        
    def getclient(self):
        return self.client        
        
        
