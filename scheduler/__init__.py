import binascii
from cacheconnector import Cachemanager
import calendar
from scheduler.responseformatter import ResponseFormatter
import time
from tornado import gen
import tornado.gen
from tornado.httpclient import AsyncHTTPClient
from tornado.httpclient import HTTPRequest
from tornado.ioloop import PeriodicCallback, IOLoop
from tornado.web import asynchronous
import ujson, logging, re, feedparser

from helper import *


# ./redis-cli SCRIPT LOAD "$(cat livescore.lua)"
class CricketSchedular(ResponseFormatter):
    
    __slots__ = ['config', 'matchid', 'crc_hash', 'responsejson', 'callback', 'ball','http_client'
                 'match_stats', 'commonstats', 'inningboard', 'required_rate', 'match_break', 'messageconnector']
    
        
    def __init__(self, config, matchid, futuretime, responsejson, messageconnector):
        self.config = config
        self.matchid = matchid
        self.messageconnector = messageconnector
        self.http_client = tornado.httpclient.AsyncHTTPClient()

        self.crc_hash = 0
        self.ball = 0
        self.match_break = False
        
        
        self.responsejson = None
        self.match_stats = None
        self.commonstats = None
        self.inningboard = None
        self.required_rate = None
        self.loadjson(responsejson)
        self.settimecall(futuretime)
    
    def getresponse(self):
        return self.responsejson
    
    
    def setresponse(self, response):
        if response.code != 304:
            crchash = self.crc32_from_bytes(response.body)
            if self.crc_hash != crchash:
                self.crc_hash = crchash
                self.responsejson = ujson.loads(response.body)
                self.matchended()
                self.matchcenter()


    def get_ball_status(self):
        return {"common":self.getcommon_stats(),
                         "stats":self.getmatch_stats(),
                         "rr":self.getrequired_rate(),
                         "inning":self.getinningsboard(),
                         "is_break":self.ismatchbreak()
                         }

    def ismatchbreak(self):
        return  self.match_break


    def getcommon_stats(self):
        return self.commonstats
    
    def getmatch_stats(self):
        return self.match_stats
    
    def getrequired_rate(self):
        return self.required_rate
    
    def getinningsboard(self):
        return self.inningboard
    
    
    
    """
    update the ball score and send it to pika
    """     
    def matchcenter(self):

        common_batting, common_bowling, common_innings = None, None, None

        matchcenter = self.responsejson["centre"]
        
        batting = self.getvalue(matchcenter, "batting")
        if batting != None:
            batting = [self.getbatmanstat(batsman) for batsman in batting]
        
        bowling = self.getvalue(matchcenter, "bowling")
        if bowling != None:
            bowling = [self.getbowlingstat(bowler) for bowler in bowling]
        
        self.match_stats = {"bowling":bowling, "batting":batting}


        fows = self.getvalue(matchcenter, "fow")
        if fows != None:
            fows = self.getfow(fows)


        
        common = self.responsejson["centre"].get("common" , None)
        if common != None:

            common_batting = self.getvalue(common, "batting")
            if common_batting != None:
                common_batting = [self.getcommonbatsman(batsman) for batsman in common_batting]

            common_bowling = self.getvalue(common, "bowling")
            if common_bowling != None:
                common_bowling = [self.getcommonbowler(bowler) for bowler in common_bowling]
            
            common_innings = self.getvalue(common, "innings")
            if common_innings != None:
#                 common_innings = [self.getcommoninning(inning) for inning in common_innings]
                common_innings = self.getcommoninning(common_innings)

            self.commonstats = {"batting":common_batting, "bowling":common_bowling,
                                "innings":common_innings,
                                 "fow":fows}
             
        self.getballstatus(self.responsejson["comms"])
        self.sendmessage(self.get_ball_status())


    
    """Send message to rabbitmq"""
    def sendmessage(self, ballstat):
        logging.info(ballstat)
        self.messageconnector.sendmessage(self.matchid, ballstat)
#         print ballstat
    
    
    
    """
    get current ball score
    """
    def getballstatus(self, commentries):
        if len(commentries) > 0:
            ball = commentries[0]["ball"][0]
            ball = self.getballstat(ball)
#             self.sendmessage(ball)
            if(len(commentries) > 1):
                self.required_rate = commentries[1].get("required_string", None)
            self.setinnings()
            

            
    def setinnings(self):
        team = {}
        team[self.responsejson["match"]["team1_id"]] = self.responsejson["match"]["team1_name"]
        team[self.responsejson["match"]["team2_id"]] = self.responsejson["match"]["team2_name"]
        innings = self.responsejson["innings"]
        i = []
        for inning in innings:
            i.append(self.getinning(inning, team))
        self.inningboard = {"innings":i}
    
            
    def loadjson(self, responsejson):
        self.responsejson = responsejson
    
    
    def settimecall(self, futuretime):
        self.dohttprequest()
        if futuretime != None and futuretime > calendar.timegm(time.gmtime()):
            self.callback = PeriodicCallback(self.matchstart, futuretime - calendar.timegm(time.gmtime()))
        else:
            self.callback = PeriodicCallback(self.matchstart, self.config.get_ball_time())
        self.callback.start()
                
                
    def matchstart(self):
        self.callback.stop()
        self.callback = PeriodicCallback(self.ballstart, self.config.get_ball_time())
        self.callback.start()
        
    @gen.coroutine
    def dohttprequest(self):
        url = self.config.get_root_url() + self.matchid + ".json"
        requesturl = HTTPRequest(url, connect_timeout=5000, request_timeout=5000)
        response = yield tornado.gen.Task(self.http_client.fetch, requesturl)
        logging.info("{0}: {1}".format(response.effective_url, response.code))
        if response.error:
            logging.error("{0} url {1}".format(response.effective_url, response.error))
        else:
            self.setresponse(response)
    
    @gen.coroutine
    def ballstart(self):
        self.ball += 1
        self.dohttprequest()
        if self.ball == 6:
            self.ball = 0
            self.callback.stop()
            yield gen.sleep(self.config.get_over_time())
#             self.callback = PeriodicCallback(self.matchstart, self.config.get_over_time())
            self.callback.start()
            
    def matchended(self):
        match_break = self.responsejson["live"].get("break", "")
        if match_break != "":
            self.setbreak(match_break)
        result_name = self.responsejson["match"].get("result_name", "")
        if result_name != "":
            self.stopschedular()
        
    
    def stopschedular(self):
        self.callback.stop()            
        
    def crc32_from_bytes(self, content):
        buf = (binascii.crc32(content) & 0xFFFFFFFF)
        return "%08X" % buf    
                
    @gen.coroutine
    def setbreak(self, break_status):
        if break_status != "":
            self.match_break = True
            self.callback.stop()
            if "stumps" in break_status:
                yield gen.sleep(self.config.get_day_time())
            elif "rain" in break_status:
                yield gen.sleep(self.config.get_rain_break_time())    
            else:
                yield gen.sleep(self.config.get_break_time())
            self.match_break = False
            self.callback = PeriodicCallback(self.ballstart, self.config.get_ball_time())
            self.callback.start()     
            
            
            
