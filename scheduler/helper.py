'''
Created on May 30, 2015

@author: mithu
'''
import sys

def getfilecontents(filename):
    with open(filename, "r") as configfile:
        data = configfile.read().replace('\n', '')
    return data    

def getargs():
    if len(sys.argv) < 2:
        return "config.json"
    else:
        return sys.argv[1]

def convert_ascii(inputvalue):
    if isinstance(inputvalue, dict):
        return {convert_ascii(key): convert_ascii(value) for key, value in inputvalue.iteritems()}
    elif isinstance(inputvalue, list):
        return [convert_ascii(element) for element in inputvalue]
    elif isinstance(inputvalue, unicode):
        return inputvalue.encode('ascii')
    else:
        return inputvalue

