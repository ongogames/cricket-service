'''
Created on Jun 5, 2015

@author: mithran
'''

class ResponseFormatter(object):
        
    def getfow(self, fows):
        fow_response = []
        for fow in fows:
            player = fow.get("player", None)
            if(len(player) > 0):
                player = player[0]
            fow_response.append({"overs":fow["overs"], "runs":fow["runs"], "player":player})
        return fow_response    
            
    def getcommoninning(self, inning):
        return self.iterate_loop(inning, ["overs", "run_rate", "runs", "target", "wicket"])
    
    
#     {"overs":inning.get("overs", None), "run_rate":inning.get("run_rate", None), "runs":inning.get("runs", None)
#          , "target":inning.get("target", None), "wicket":inning.get("wicket", None)}    
        
    def getvalue(self, dictionary, key):
        if key in dictionary:
            return dictionary[key]
        return None
    
    def iterate_loop(self, dictionary, lists):
        d = dict()
        for key in lists:
            if key in dictionary:
                d[key] = dictionary[key]
            else:
                d[key] = None
        return d
        
    def getcommonbatsman(self, batsman):
        return self.iterate_loop(batsman, ["balls_faced", "hand", "known_as", "notout", "runs"])
    
#     {"balls_faced":batsman["balls_faced"], "hand":batsman["hand"],
#                 "known_as":batsman["known_as"],
#                 "notout":batsman["notout"], "runs":batsman["runs"]}
        
        
        
    def getcommonbowler(self, bowler):
        return self.iterate_loop(bowler, ["conceded", "hand", "known_as", "maidens", "overs", "pacespin", "wickets"])
    
    
#     {"conceded":bowler["conceded"], "hand":bowler["hand"],
#                 "known_as":bowler["known_as"], "maidens":bowler["maidens"],
#                 "overs":bowler["overs"], "pacespin":bowler["pacespin"],
#                 "wickets":bowler["wickets"]}
    
    

    def getbowlingstat(self, bowler):
        return self.iterate_loop(bowler, ["bowling_style", "conceded", "economy_rate", "known_as",
                                          "live_current_name", "maidens", "overs" ])
    
#     {"bowling_style":bowler["bowling_style"], "conceded":bowler["conceded"],
#                 "economy_rate":bowler["economy_rate"], "known_as":bowler["known_as"],
#                 "live_current_name":bowler["live_current_name"], "maidens":bowler["maidens"],
#                 "overs":bowler["overs"]}
    
    def getinning(self, inning, team):
        return {"event":self.getvalue(inning, "event_name"), "extras":self.getvalue(inning, "extras"),
             "batting":team[inning["batting_team_id"]], "byes":self.getvalue(inning, "byes"),
             "ball_limit":self.getvalue(inning, "ball_limit"), "balls":self.getvalue(inning, "balls"),
             "lead":self.getvalue(inning, "lead"), "legbyes":self.getvalue(inning, "legbyes"),
             "noballs":self.getvalue(inning, "noballs"), "over_limit":self.getvalue(inning, "over_limit"),
             "over_limit_run_rate":self.getvalue(inning, "over_limit_run_rate"), "wickets":self.getvalue(inning, "wickets"),
             "wides":self.getvalue(inning, "wides"), "target":self.getvalue(inning, "target"),
             "runs":self.getvalue(inning, "runs"), "run_rate":self.getvalue(inning, "run_rate"),
             "minutes":self.getvalue(inning, "minutes"), "innings_number":self.getvalue(inning, "innings_number")} 
        
           
    def getballstat(self, ball):
        return {"over":self.getvalue(ball, "overs_unique"), "actual_over":self.getvalue(ball, "overs_actual"),
             "players":self.getvalue(ball, "players"), "event":self.getvalue(ball, "event"),
             "dismissal":self.getvalue(ball, "dismissal"), "speed_kph":self.getvalue(ball, "speed_kph"),
             "speed_mph":self.getvalue(ball, "speed_mph"), "text":self.getvalue(ball, "text"),
             "post_text":self.getvalue(ball, "post_text"), "pre_text":self.getvalue(ball, "pre_text")}

    
            
    def getbatmanstat(self, batsman):
        return self.iterate_loop(batsman, ["balls_faced", "batting_style", "known_as", "live_current_name", "strike_rate", "runs"])
    
#     {"balls_faced":batsman["balls_faced"], "batting_style":batsman["batting_style"],
#                 "known_as":batsman["known_as"], "live_current_name":batsman["live_current_name"],
#                 "strike_rate":batsman["strike_rate"], "runs":batsman["runs"]}    
    
