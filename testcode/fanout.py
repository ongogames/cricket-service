'''
Created on Jun 10, 2015

@author: mithran
'''
import pika
import sys





credentials = pika.PlainCredentials("guest", "guest")
conn_params = pika.ConnectionParameters("localhost",
                                        credentials = credentials)
connection = pika.BlockingConnection(conn_params) 

channel = connection.channel()

channel.exchange_declare(exchange='logs',
                         type='fanout',passive=False,
                         auto_delete=False)

message = ' '.join(sys.argv[1:]) or "info: Hello World!"
channel.basic_publish(exchange='logs',
                      routing_key='',
                      body=message)

print " [x] Sent %r" % (message,)
connection.close()