


class Configuration(object):
    
    __slots__ = ['config']
    
    def __init__(self, config):
        self.config = config
        
    def get_ball_interval(self):
        return self.config["ballinterval"]
    
    def get_over_interval(self):
        return self.config["overinterval"]
    
    def get_live_interval(self):
        return self.config["liveinterval"]

    def get_static_path(self):
        return self.config["staticpath"]
    
    def get_rootstatic_path(self):
        return self.config["rootstaticpath"]
        
    def get_logpath(self):
        return self.config["logpath"]
    
    def get_live_url(self):
        return  self.config["live_url"]
    
    def get_root_url(self):
        return self.config["root_url"]
    
    def get_root(self):
        return self.config["root"]

    def get_ball_time(self):
        return self.config["ball_time"]
    
    def get_over_time(self):
        return self.config["over_time"]

    def get_connect_timeout(self):
        return self.config["connect_timeout"]

    def get_request_timeout(self):
        return self.config["request_timeout"]
    
    def get_day_time(self):
        return self.config["day_time"]
    
    def get_break_time(self):
        return self.config["break_time"]

    def get_rain_break_time(self):
        return self.config["rain_break_time"]
    
    def get_varnish_host(self):
        return self.config["varnish_host"]
    
    def get_varnish_live_request(self):
        return self.config["varnish_live_request"]

    def get_varnish_ball_request(self):
        return self.config["varnish_ball_request"]
    
    def get_port(self):
        return self.config["port"]

    def get_log_filesize(self):
        return self.config["log_file_size"]
     
    def get_log_stderr(self):
        return  self.config["log_to_stderr"].lower() == "true"

    def get_rabbitmq_host(self):
        return self.config["rabbitmq"]["host"]
    
    def get_rabbitmq_port(self):
        return self.config["rabbitmq"]["port"]
    
    def get_rabbitmq_vhost(self):
        return self.config["rabbitmq"]["vhost"]
    
    def get_rabbitmq_username(self):
        return self.config["rabbitmq"]["username"]
    
    def get_rabbitmq_password(self):
        return self.config["rabbitmq"]["password"]
    
    

